<P align="center"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></p>

# Final Project 1

# Kelompok 12

## Anggota Kelompok

- Asep Oemar Alfaruqi
- Dwiki Rizki Ilham P
- Fajar Putra Pamungkas

# Tema Project

Perpustakaan

# ERD

<p>
    <img src="https://gitlab.com/asepoemarGL/perpustakaan/-/raw/master/public/images/ERD%20Project1.png">
</p>

# Link Vidio

link Demo Aplikasi :

Link Deploy (opsional) : https://git.heroku.com/projectkl12.git
