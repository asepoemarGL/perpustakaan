@extends('layout.main')


@section('title', 'Laravel - MiniPerpustakaan')

@push('style')
    <style>
        #hideMe {
        -moz-animation: cssAnimation 0s ease-in 5s forwards;
        /* Firefox */
        -webkit-animation: cssAnimation 0s ease-in 5s forwards;
        /* Safari and Chrome */
        -o-animation: cssAnimation 0s ease-in 5s forwards;
        /* Opera */
        animation: cssAnimation 0s ease-in 5s forwards;
        -webkit-animation-fill-mode: forwards;
        animation-fill-mode: forwards;
        }
        @keyframes cssAnimation {
            to {
                width:0;
                height:0;
                overflow:hidden;
            }
        }
        @-webkit-keyframes cssAnimation {
            to {
                width:0;
                height:0;
                visibility:hidden;
            }
        }
  </style>
@endpush


@section('content')


<div class="container">
    <div class="jumbotron">
        {{-- id='hideMe' itu untuk menghilangkan tag setelah 5 detik --}}
        {{-- stylenya ada di head main.blade.php --}}
        <h1 class="display-4">Selamat Datang!</h1>
        <p class="lead" id='hideMe'>INI TUGAS PROJECT LARAVEL</p>
        <hr class="" id='hideMe'>

        <div class="row d-flex justify-content-around">
            @forelse ($buku as $key=>$value)
                <div>
                    <div class="card" style="width: 15rem;">
                        <img class="card-img-top" src="{{asset('images/'.$value->cover)}}" alt="...">
                        <div class="card-body">
                            <span class="badge badge-info">{{$value->kategori->nama}}</span>
                            <h5>{{$value->judul}}, {{$value->tahun}}</h5>
                            <p class="card-text">{{ Str::limit($value->deskripsi, $limit = 30, $end='...')}}</p>
                        </div>
                    </div>
                </div> 
            @empty
                <H2>Tidak ada data</H2>
            @endforelse
        </div>

    </div>
</div>


@endsection