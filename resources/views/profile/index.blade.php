@extends('layout.master')

@section('judul')
@endsection

@section('judul2')
    Update Profile
@endsection

@section('content')


<div>
    <h2>Update Data Profile</h2>
        <form action="/profile/{{$profile->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" value="{{$profile->user->name}}" disabled>
            </div>
            <div class="form-group">
                <label for="title">Email</label>
                <input type="text" class="form-control" value="{{$profile->user->email}}" disabled>
            </div>
            <div class="form-group">
                <label for="title">Umur</label>
                <input type="number" class="form-control" value="{{$profile->umur}}" name="umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Nomor Telepon</label>
                <input type="text" class="form-control" value="{{$profile->no_telp}}" name="no_telp">
                @error('no_telp')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Alamat</label>
                {{-- <input type="text" class="form-control" name="alamat" placeholder="Masukkan alamat"> --}}
                <textarea name="alamat" class="form-control" rows="8">{{$profile->alamat}}</textarea>
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
</div>

@endsection