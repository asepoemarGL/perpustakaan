@extends('layout.master')

@section('judul')
@endsection

@section('judul2')
    List Data Buku
    
@endsection

@section('content')
<a href="/buku/create" class="btn btn-primary mb-2">Tambah</a>

@if (session()->has('massage'))
    <div class="alert alert-success">
        {{session()->get('message')}}
    </div>
@endif

<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Cover</th>
        <th scope="col">Content</th>
        {{-- <th scope="col">Pengarang</th>
        <th scope="col">Tahun</th>
        <th scope="col">Jumlah</th> --}}
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($buku as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>
                    <img src="{{asset('images/'.$value->cover)}}" alt="" class="img-thumbnail" style="width: 8rem;">
                </td>
                <td>
                    <H5>{{$value->judul}}</H5>
                    <p>Pengarang: {{$value->pengarang}}</p>
                    <p>Tahun - {{$value->tahun}}</p>
                    <p>Jumlah Buku: {{$value->jumlah}}</p>
                    <p>Kategori: {{$value->kategori->nama}}</p>
                </td>
                {{-- <td>{{$value->pengarang}}</td>
                <td>{{$value->tahun}}</td>
                <td>{{$value->jumlah}}</td> --}}
                <td>
                    <form action="/buku/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="/buku/{{$value->id}}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                        <a href="/buku/{{$value->id}}/edit" class="btn btn-primary"><i class="fas fa-edit"></i></a>
                        <button type="submit" class="btn btn-danger my-1" value="Delete" onclick="return confirm('Are You Sure?')">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>Tidak ada data</td>
            </tr>  
        @endforelse   
    </tbody>
</table>

@endsection