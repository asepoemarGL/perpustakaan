@extends('layout.master')

@section('judul')
@endsection

@section('judul2')
    Tambah Buku
@endsection

@section('content')


<div>
    <h2>Tambah Data buku</h2>
        <form action="/buku" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">Judul Buku</label>
                <input type="text" class="form-control" name="judul" placeholder="Masukkan Judul">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Pengarang</label>
                <input type="text" class="form-control" name="pengarang" placeholder="Masukkan Pengarang">
                @error('pengarang')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Tahun Dibuat</label>
                <input type="number" class="form-control" name="tahun" placeholder="Masukkan tahun, contoh: 2021">
                @error('tahun')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Jumlah</label>
                <input type="number" class="form-control" name="jumlah" placeholder="Masukkan Jumlah Buku">
                @error('jumlah')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Cover (hanya file .jpeg, .png dan .jpg)</label>
                <input type="file" class="form-control" name="cover">
                @error('cover')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">kategori</label>
                <select name="kategori_id" class="form-control">
                    <option value="">--Pilih Kategori--</option>
                    @foreach ($kategori as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                    @endforeach
                </select>
                @error('kategori_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Posisi</label>
                <select name="posisi_id" class="form-control">
                    <option value="">--Pilih Posisi Penyimpanan--</option>
                    @foreach ($posisi as $item)
                        <option value="{{$item->id}}">{{$item->posisi}}</option>
                    @endforeach
                </select>
                @error('posisi_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Deskripsi</label>
                {{-- <input type="text" class="form-control" name="deskripsi" placeholder="Masukkan deskripsi"> --}}
                <textarea name="deskripsi" class="form-control" rows="8" placeholder="Masukkan Deskripsi"></textarea>
                @error('deskripsi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>

@endsection