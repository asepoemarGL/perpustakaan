@extends('layout.master')

@section('judul')
@endsection

@section('judul2')
    Show Detail Buku
@endsection

@section('content')


<div>
    <table class="tabel">
        <tr>
            <td>
                <img src="{{asset('images/'.$buku->cover)}}" alt="" class="img-thumbnail" style="width: 18rem;">
            </td>
            <td>
            </td>
            <td>
                <H2>{{$buku->judul}}</H2>
                <p>Pengarang: {{$buku->pengarang}}</p>
                <p>Tahun - {{$buku->tahun}}</p>
                <p>{{$buku->deskripsi}}</p>
                <p>Jumlah Buku: {{$buku->jumlah}}</p>
                <p>Kategori: {{$buku->kategori->nama}}</p>
                <p>Posisi Buku: {{$buku->posisi->posisi}}</p>

                @forelse ($buku->transaksi as $item)
                    <p>Peminjam: <b>{{$item->user->name}} </b> <br> pinjam {{$item->tanggal_pinjam}} - kembali {{$item->tanggal_kembali}}</p>
                @empty
                    <h5>Tidak ada Peminjaman</h5>
                @endforelse

                <form action="/transaksi" class="my-4" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="title">Tanggal Pinjam</label>
                        <input type="hidden" value="{{$buku->id}}" name="buku_id">
                        <input type="text" class="form-control" name="tanggal_pinjam" placeholder="contoh: 21/05/2021">
                        @error('tanggal_pinjam')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="body">Tanggal Kembali</label>
                        <input type="text" class="form-control" name="tanggal_kembali" placeholder="max 7 hari dari pinjam. contoh: 28/05/2021">
                        @error('tanggal_kembali')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

                <a href="/buku" class="btn btn-info">Kembali</a>
            </td>
            <td>
        </tr>
    </table>
</div>

@endsection