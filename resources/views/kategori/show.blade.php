@extends('layout.master')

@section('judul')
@endsection

@section('judul2')
    Show Buku
@endsection

@section('content')


<div>
    <h1>{{$kategori->nama}}</h1>
</div>

<div>
    <h3>List Buku</h3>
</div>

<div class="row d-flex justify-content-around">
            @forelse ($kategori->buku as $value)
                <div>
                    <div class="card" style="width: 15rem;">
                        <img class="card-img-top" src="{{asset('images/'.$value->cover)}}" alt="...">
                        <div class="card-body">
                            <h5>{{$value->judul}}, {{$value->tahun}}</h5>
                            <p class="card-text">{{ Str::limit($value->deskripsi, $limit = 30, $end='...')}}</p>
                        </div>
                    </div>
                </div> 
            @empty
                <H2>Tidak ada data</H2>
            @endforelse
        </div>
<a href="/kategori" class="btn btn-info">Kembali</a>


@endsection