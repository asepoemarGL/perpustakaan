@extends('layout.master')

@section('judul')
@endsection

@section('judul2')
    List Data Kategori
    
@endsection

@section('content')
<a href="/kategori/create" class="btn btn-primary mb-2">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($kategori as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>
                            <form action="/kategori/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/kategori/{{$value->id}}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                                <a href="/kategori/{{$value->id}}/edit" class="btn btn-primary"><i class="fas fa-edit"></i></a>
                                <button type="submit" class="btn btn-danger my-1" value="Delete" onclick="return confirm('Are You Sure?')">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>Tidak ada data</td>
                    </tr>  
                @endforelse   
            </tbody>
        </table>

@endsection