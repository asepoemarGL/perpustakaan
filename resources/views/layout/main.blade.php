<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

  @stack('style')
  
  <title>@yield('title')</title>

</head>
<body>
    <div class="container mt-2">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="/">PERPUSTAKAAN</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    
            {{-- <span class="navbar-toggler-icon"></span> --}}
    
          </button>
        
          <div class="collapse navbar-collapse d-flex justify-content-end" id="navbarNavAltMarkup">
            <div class="navbar-nav">
              @auth
                <a class="nav-item nav-link" href="/buku">Manage Data</a>
              @endauth
              <a class="nav-item nav-link">|</a>
              @guest
                <a class="nav-item nav-link" href="/login">Login</a>
              @endguest
              @auth
                <button type="button" class="btn btn-danger" href="{{ route('logout') }}" onclick="event.preventDefault(); 
                document.getElementById('logout-form').submit();">
                  Logout
                </button>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
              @endauth
          </div>
        </nav>
        {{-- <div class=""> --}}

            
            {{-- </div> --}}
        </div>
        
        @yield('content')

    {{-- <div class="container bg-dark text-center">
        footer
    </div> --}}

</body>
</html>
