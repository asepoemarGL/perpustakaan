<?php

// use DB;
use App\Buku;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $buku = Buku::all();
    return view('index',compact('buku'));

});



Auth::routes();


Route::middleware(['auth'])->group(function () {

    Route::get('/master', function () {
        return view('layout.master');
    });
    
    //CRUD tabel kategori
    Route::resource('kategori', 'KategoriController');
    
    //CRUD tabel posisi
    Route::resource('posisi', 'PosisiController');
    
    //CRUD tabel buku
    Route::resource('buku', 'BukuController');

    Route::resource('profile', 'ProfileController')->only(['index','update']);

    Route::resource('transaksi', 'TransaksiController')->only(['store']);

});

// Route::get('/home', 'HomeController@index')->name('home');
