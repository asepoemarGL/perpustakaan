<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaksi;

use Illuminate\Support\Facades\Auth;

class TransaksiController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'tanggal_pinjam' => 'required',
            'tanggal_kembali' => 'required',
        ]);

        $transaksi = new Transaksi;

        $transaksi->tanggal_pinjam = $request->tanggal_pinjam;
        $transaksi->tanggal_kembali = $request->tanggal_kembali;
        $transaksi->user_id = Auth::id();
        $transaksi->buku_id = $request->buku_id;

        $transaksi->save();

        return redirect()->back();
    }
}
