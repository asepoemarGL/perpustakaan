<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Buku;
use File; //untuk update data cover buku

class BukuController extends Controller
{
    //ini untuk meng-auth seluruh fungsi controller kecuali index dan show 
    public function __construct()
    {
        $this->middleware('auth')->except('index','show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buku = Buku::all();
        return view('buku.index',compact('buku'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //untuk mendapatkan data kategori dan Posisi
        $posisi = DB::table('tbl_posisi')->get();
        $kategori = DB::table('tbl_kategori')->get();
        return view('buku.create',compact('kategori','posisi'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'pengarang' => 'required',
            'tahun' => 'required',
            'deskripsi' => 'required',
            'jumlah' => 'required',
            'kategori_id' => 'required',
            'posisi_id' => 'required',
            'cover' => 'required|image|mimes:jpeg,png,jpg',
        ]);

        $namaCover = time().'.'.$request->cover->extension();  
     
        $request->cover->move(public_path('images'), $namaCover);

        $buku = new Buku;

        $buku->judul = $request->judul;
        $buku->pengarang = $request->pengarang;
        $buku->tahun = $request->tahun;
        $buku->deskripsi = $request->deskripsi;
        $buku->jumlah = $request->jumlah;
        $buku->kategori_id = $request->kategori_id;
        $buku->posisi_id = $request->posisi_id;
        $buku->cover = $namaCover;

        $buku->save();
        return redirect(route('buku.index'))->with('message','Data Buku berhasil di tambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $buku = Buku::findOrFail($id);
        $posisi = DB::table('tbl_posisi')->get();
        $kategori = DB::table('tbl_kategori')->get();
        return view('buku.show', compact('buku','posisi','kategori'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $buku = Buku::findOrFail($id);
        $posisi = DB::table('tbl_posisi')->get();
        $kategori = DB::table('tbl_kategori')->get();
        return view('buku.edit',compact('buku','kategori','posisi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'pengarang' => 'required',
            'tahun' => 'required',
            'deskripsi' => 'required',
            'jumlah' => 'required',
            'kategori_id' => 'required',
            'posisi_id' => 'required',
            'cover' => 'image|mimes:jpeg,png,jpg',
        ]);

        if ($request->has('cover')) {
            $buku = Buku::find($id);
            
            $path = "images/";
            File::delete($path . $buku->cover);

            $namaCover = time().'.'.$request->cover->extension();  
     
            $request->cover->move(public_path('images'), $namaCover);

            $buku->judul = $request->judul;
            $buku->pengarang = $request->pengarang;
            $buku->tahun = $request->tahun;
            $buku->deskripsi = $request->deskripsi;
            $buku->jumlah = $request->jumlah;
            $buku->kategori_id = $request->kategori_id;
            $buku->posisi_id = $request->posisi_id;
            $buku->cover = $namaCover;

            $buku->save();
            return redirect('/buku');

        }else{
            $buku = Buku::find($id);
            $buku->judul = $request->judul;
            $buku->pengarang = $request->pengarang;
            $buku->tahun = $request->tahun;
            $buku->deskripsi = $request->deskripsi;
            $buku->jumlah = $request->jumlah;
            $buku->kategori_id = $request->kategori_id;
            $buku->posisi_id = $request->posisi_id;

            $buku->save();
            return redirect('/buku');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $buku = Buku::find($id);

        $path = "images/";
        File::delete($path . $buku->cover);

        $buku->delete();

        return redirect('/buku');
    
    }
}
