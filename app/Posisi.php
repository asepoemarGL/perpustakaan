<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posisi extends Model
{
    protected $table = 'tbl_posisi';

    protected $fillable = ['posisi'];

    public function buku()
    {
        return $this->hasMany('App\Buku');
    }
}
