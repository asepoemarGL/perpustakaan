<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = 'tbl_buku';

    protected $fillable = ['judul','pengarang','tahun','deskripsi','jumlah','cover','kategori_id','posisi_id'];

    public function kategori()
    {
        return $this->belongsTo('App\Kategori');
    }
    
    public function posisi()
    {
        return $this->belongsTo('App\Posisi');
    }

    public function transaksi()
    {
        return $this->hasMany('App\Transaksi');
    }
}
