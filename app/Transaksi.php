<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = 'tbl_transaksi';

    protected $fillable = ['user_id','buku_id','tanggal_pinjam','tanggal_kembali'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function buku()
    {
        return $this->belongsTo('App\Buku');
    }
}
