<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'tbl_kategori';

    protected $fillable = ['nama'];

    public function buku()
    {
        return $this->hasMany('App\Buku');
    }
}
