<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBukuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_buku', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('judul', 45 );
            $table->string('pengarang', 45 );
            $table->string('tahun', 45 );
            $table->text('deskripsi');
            $table->integer('jumlah');
            $table->string('cover', 45 );
            $table->unsignedBigInteger('kategori_id');
            $table->foreign('kategori_id')->references('id')->on('tbl_kategori');
            $table->unsignedBigInteger('posisi_id');
            $table->foreign('posisi_id')->references('id')->on('tbl_posisi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_buku');
    }
}
